from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView
from receipts.models import Account, Receipt, ExpenseCategory
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt

    def get_queryset(self):
        return super().get_queryset().filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields["account"].queryset = Account.objects.filter(
            owner=self.request.user
        )
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=self.request.user
        )
        return form

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    context_object_name = "category_list"

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user)


class ExpenseCategoryCreateView(CreateView):
    model = ExpenseCategory
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "receipts/account_form.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")
