from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.views.generic import CreateView

# Create your views here.


def create_user(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    return render(
        request,
        "registration/signup.html",
        context={"form": form},
    )


# class UserCreateView(CreateView):
#     form_class = UserCreationForm
#     template_name = "registration/signup.html"
