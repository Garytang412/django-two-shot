from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

# Register your models here.
admin.site.register([Account, Receipt, ExpenseCategory])
